FROM erlang:26.2.3-alpine

RUN apk add --no-cache \
            bash \
            coreutils \
            curl

COPY avassa-mount-secrets.sh /
COPY ams/ebin /ams/ebin
COPY deps/gun /gun
COPY deps/cowlib /cowlib
COPY deps/jsx /jsx

WORKDIR /
CMD ["/avassa-mount-secrets.sh"]
