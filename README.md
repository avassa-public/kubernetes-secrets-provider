# Avassa Secrets Provider for Kubernetes

The purpose of this repository is to provide some means of consuming
Avassa Strongbox secrets from a Kubernetes pod.

Building this module requires a working Erlang installation.  It will
result in a container that can be used as a init container in
Kubernetes in order to access secrets stored in Strongbox. It can be
used to mount the secrets as files, and to populate Kubernetes secrets
with the contents of a Strongbox vault secret.

The container will read k8s annotations. There are a number of
required fields to connect to the avassa control tower:

```
  avassa.io/api-host
  avassa.io/tenant
  avassa.io/service
  avassa.io/role
```

It may look like this in a pod specification

```
  apiVersion: v1
  kind: Pod
  metadata:
    name: auth-demo
    annotations:
      avassa.io/api-host: api.telco.avassa.io
      avassa.io/tenant: telco
      avassa.io/service: minikube
      avassa.io/role: default
```

Secrets can be mapped either to files or into the k8s secrets
store (and into environment variables).

## Mapping into files

There are several ways a Strongbox secret may be mapped into
files. Suppose we have a vault called 'credentials' in
Strongbox with the following secret:

```
   name: admin
   data:
     password: verysecret
     user: jb@bevemyr.com
```

This secret can be mapped to file(s) in a few different
ways.

1. A file can be created for each data key in the secret. The
   following annotation is added to the pod

```
      avassa.io/vault.<vault-name>.<secret-name>: files
```

   eg

```
      avassa.io/vault.credentials.admin: files
```

   The result will be the files 'user' and 'password' being created.

2. The value of a specific key can be stored in a named file.
   An annotation on the following format is added:

```
      avassa.io/vault.<vault-name>.<secret-name>.<key-id>: <file-name>
```

   eg

```
      avassa.io/vault.credentials.admin.user: user.txt
```

3. The entire secrets dictionary can be stored as a json structure
   in a named file. An annotation on the following format is added:
      avassa.io/vault.<vault-name>.<secret-name>: json/<file-name>
   eg
      avassa.io/vault.credentials.admin: json/credentials.json


## Mapping into k8s secrets

The mounter can also be used to populate k8s secrets. The
procedure is to create a k8s secret with a specific filed
named 'strongbox-map'. The field should contain a mapping
specification on the format

```
    <secret-key>: <vault-name>/<secret-name>/<key-id>
```

The result will be that the k8s secret will be expanded
with the <secret-key> data where the value will be read
from the Strongbox vault.

eg

```
   apiVersion: v1
   kind: Secret
   metadata:
     name: db-credentials
   type: Opaque
   stringData:
     strongbox-map: |-
       username: credentials/admin/user
       password: credentials/admin/password
```
