### include this from lib/*/Makefile

SELF_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
include $(SELF_DIR)/common.mk

DEPS_DIR = $(TOP_DIR)/deps
LIB_DIR = $(TOP_DIR)/lib

dep_lux = git https://github.com/jbevemyr/lux.git master

TEST_DEPS += lux

# use .app.env with erlang env, instead of .app.src
PROJECT_ENV_FILE = src/$(PROJECT).app.env
ifneq ($(wildcard $(PROJECT_ENV_FILE)),)
PROJECT_ENV = $(subst \n,$(newline),$(shell cat $(PROJECT_ENV_FILE) | sed -e 's/$$/\\n/;'))
endif

ERLC_USE_SERVER = true
export ERLC_USE_SERVER

EUNIT_ERL_OPTS += -kernel logger_level warning
EUNIT_OPTS = verbose,{print_depth,9999},{report,{eunit_surefire,[{dir,"."}]}}

include $(TOP_DIR)/mk/erlang.mk

ifeq ($(NO_LOCAL_BUILD_DEPS),)
LOCAL_BUILD_DEPS_DIRS += $(addprefix $(LIB_DIR)/, $(LOCAL_BUILD_DEPS))
endif

echo-local-build-deps:
	@echo $(LOCAL_BUILD_DEPS)

deps::
	$(verbose) set -e; for dep in $(LOCAL_BUILD_DEPS_DIRS); do \
	  env -u ERLANG_MK_TMP $(MAKE) -C $$dep; \
	done


ebin/$(PROJECT).app:: $(wildcard $(PROJECT_ENV_FILE))

ERLC_OPTS += -D APP=$(PROJECT)
TEST_ERLC_OPTS += -D APP=$(PROJECT)

# ensure we have correct include path
ERLC_OPTS += -I $(LIB_DIR) -pa $(LIB_DIR)/dapid/ebin
TEST_ERLC_OPTS += -I $(LIB_DIR)

# we allow export of variables from if/case
ERLC_OPTS := $(filter-out +warn_export_vars,$(ERLC_OPTS))
TEST_ERLC_OPTS := $(filter-out +warn_export_vars,$(TEST_ERLC_OPTS))

## add targets for yang compilation

YANG_FILES = $(filter %.yang,$(ALL_SRC_FILES))
YANG_ERL_FILES = $(patsubst src/%.yang, src/yy-%.erl, $(YANG_FILES))
ERL_FILES += $(YANG_ERL_FILES)
YANG_BEAM_FILES = $(patsubst src/%.yang, ebin/yy-%.beam, $(YANG_FILES))
YANG_HRL_FILES = $(patsubst src/%.yang, src/yy-%.hrl, $(YANG_FILES))

app:: $(YANG_BEAM_FILES) $(YANG_HRL_FILES)

yangbeam: $(YANG_BEAM_FILES)

YANGER_OPTS ?=

YANGER_YY_DIR = $(LIB_DIR)/yanger_yy/ebin
YY_DIR = $(LIB_DIR)/yy/ebin
COMMON_SRC = $(LIB_DIR)/common/src
SYST_SRC = $(LIB_DIR)/syst/src
VOLGA_SRC = $(LIB_DIR)/volga/src

yang_verbose_0 = @echo " YANGER " $(filter %.yang,$<);
yang_verbose_2 = set -x;
yang_verbose = $(yang_verbose_$(V))

src/yy-%.erl: src/%.yang ../yy/include/yy.hrl $(YANGER_YY_DIR)/yanger_yy.beam
	$(yang_verbose) $(YANGER) -Werror -P $(YANGER_YY_DIR) --pz $(YY_DIR) \
	-p $(COMMON_SRC) -p $(VOLGA_SRC) -p $(SYST_SRC) \
	-f yy $(YANGER_OPTS) -o $@ $<

src/yy-%.hrl: ebin/yy-%.beam src/%.yang $(YANGER_YY_DIR)/yy_hrl.beam
	$(gen_verbose) $(ERL) -pa $(YANGER_YY_DIR) \
	  -run yy_hrl mk_hrl $< $@ -s erlang halt

.INTERMEDIATE: $(YANG_ERL_FILES)

## if we have yang files we need yanger
ifneq ($(strip $(YANG_FILES)),)
LOCAL_BUILD_DEPS += yanger_yy

YANG_MODULES = $(patsubst src/%.yang, %, $(YANG_FILES))

app:: $(PROJECT).yang.d

$(PROJECT).yang.d: $(YANG_FILES)
	rm -f $@
	$(makedep_verbose) for m in $(YANG_MODULES); do \
		 $(YANGER) -p $(COMMON_SRC) -p $(SYST_SRC) $(YANGER_OPTS) \
		-f depend --depend-include-path --depend-double-colon \
		--depend-target src/yy-$$m.erl src/$$m.yang >> $@; \
	done

include $(wildcard $(PROJECT).yang.d)
endif

## add lux to tests target

tests:: lux

clean::
	$(gen_verbose) rm -f src/yy-*rl $(PROJECT).yang.d
	$(gen_verbose) for d in $(LUX_DIRS) ; do ( cd $$d && $(MAKE) clean ) ; done

LUX_DIRS = $(dir $(wildcard test/lux/*/Makefile))

lux: test-deps
	res=0; \
	for d in $(LUX_DIRS) ; do \
	  ( cd $$d && $(MAKE) lux ) ; res=$$(($$res + $$?)) ; \
	done ; exit $$res

.PHONY: lux
