SELF_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
TOP_DIR := $(realpath $(SELF_DIR)/..)

export LUX = $(TOP_DIR)/deps/lux/bin/lux --case_timeout 900000
export YANGER = $(TOP_DIR)/deps/yanger/bin/yanger
export YY =  $(TOP_DIR)/lib/yy/bin/yy
