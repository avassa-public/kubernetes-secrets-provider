%%  -*- erlang-mode -*-
%% SPDX-License-Identifier: Apache-2.0
%%
%% A prerequisite is that Strongbox has been configured to
%% properly recognize k8s service tokens as valid credentials.
%%

%%
%% The module will read k8s annotations. There are a number of
%% required fields to connect to the avassa control tower:
%%
%%   avassa.io/api-host
%%   avassa.io/tenant
%%   avassa.io/service
%%   avassa.io/role
%%
%% It may look like this in a pod specification
%%
%%   apiVersion: v1
%%   kind: Pod
%%   metadata:
%%     name: auth-demo
%%     annotations:
%%       avassa.io/api-host: telco.api.avassa.io
%%       avassa.io/tenant: telco
%%       avassa.io/service: minikube
%%       avassa.io/role: default
%%
%% Secrets can be mapped either to files or into the k8s secrets
%% store (and into environment variables).
%%
%% Mapping into files
%% ==================
%%
%% There are several ways a Strongbox secret may be mapped into
%% files. Suppose we have a vault called 'credentials' in
%% Strongbox with the following secret:
%%
%%    name: admin
%%    data:
%%      password: verysecret
%%      user: jb@bevemyr.com
%%
%% This secret can be mapped to file(s) in a few different
%% ways.
%%
%% 1. A file can be created for each data key in the secret. The
%%    following annotation is added to the pod
%%
%%       avassa.io/vault.<vault-name>.<secret-name>: files
%%
%%    eg
%%
%%       avassa.io/vault.credentials.admin: files
%%
%%    The result will be the files 'user' and 'password' being created.
%%
%% 2. The value of a specific key can be stored in a named file.
%%    An annotation on the following format is added:
%%
%%       avassa.io/vault.<vault-name>.<secret-name>.<key-id>: <file-name>
%%
%%    eg
%%
%%       avassa.io/vault.credentials.admin.user: user.txt
%%
%% 3. The entire secrets dictionary can be stored as a json structure
%%    in a named file. An annotation on the following format is added:
%%       avassa.io/vault.<vault-name>.<secret-name>: json/<file-name>
%%    eg
%%       avassa.io/vault.credentials.admin: json/credentials.json
%%
%%
%% Mapping into k8s secrets
%% ========================
%%
%% The mounter can also be used to populate k8s secrets. The
%% procedure is to create a k8s secret with a specific filed
%% named 'strongbox-map'. The field should contain a mapping
%% specification on the format
%%
%%     <secret-key>: <vault-name>/<secret-name>/<key-id>
%%
%% The result will be that the k8s secret will be expanded
%% with the <secret-key> data where the value will be read
%% from the Strongbox vault.
%%
%% eg
%%
%%    apiVersion: v1
%%    kind: Secret
%%    metadata:
%%      name: db-credentials
%%    type: Opaque
%%    stringData:
%%      strongbox-map: |-
%%        username: credentials/admin/user
%%        password: credentials/admin/password
%%
%%
%% Container mode
%% ==============
%%
%% The secrets provider can be run both as an init-container,
%% in order to populate files and the Kubernetes secrets store
%% before an application is started, and as a regular container
%% in order to continuously update secrets.
%%
%% The default mode is to as an init container that will exit once
%% it is done.
%%
%% The mode can be specified using the avassa.io/mode annotation
%% and can take the values init and application. In application
%% mode the container will not exit but instead remain and periodically
%% update the secrets from Strongbox. The update interval can be
%% configured using the avassa.io/update-interval setting. The interval
%% setting must be on the form [<digits>y][<digits>d][<digits>m][<digits>s].
%% The default interval is 5 minutes.
%%
%% The mode can also be set using an environment variable AVASSA_MODE,
%% and the interval can be set using the variable AVASSA_UPDATE_INTERVAL
%%


-module('ams').
-author('jb@tio').

%% Useful macro shorthands
-define(io2b, iolist_to_binary).
-define(i2l,  integer_to_list).
-define(l2i,  list_to_integer).
-define(b2l,  binary_to_list).
-define(l2b,  list_to_binary).
-define(t2b,  term_to_binary).
-define(b2t,  binary_to_term).
-define(a2l,  atom_to_list).
-define(l2a,  list_to_atom).
-define(l2ea, list_to_existing_atom).
-define(b2a,  binary_to_atom).
-define(b2ea, binary_to_existing_atom).
-define(a2b,  atom_to_binary).
-define(l2t,  list_to_tuple).
-define(t2l,  tuple_to_list).
-define(i2b,  integer_to_binary).
-define(b2i,  binary_to_integer).
-define(b2f,  binary_to_float).

-define(log(F,A), log(?MODULE,?LINE,F,A)).

-export([start/0]).

start() ->
    try
        application:ensure_all_started(gun),
        Dict = parse_annotations(),
        mount_secrets(Dict)
    catch
        X:Y:Stack ->
            ?log("Crash ~p:~p\n~p\n", [X,Y,Stack]),
            ok
    end.

mount_secrets(Dict) ->
    ?log("Mounting secrets\n", []),
    case authenticate(Dict) of
        error ->
            ?log("Failed to authenticate towards Avassa.\n", []),
            ok;
        Token ->
            Secrets = get_strongbox_secrets(Dict, Token),
            write_secrets(Dict, Secrets),
            logout(Dict, Token)
    end,
    %% ?log("done\n", []),
    case get_mode(Dict) of
        <<"init">> ->
            %% Done, exit
            ok;
        <<"application">> ->
            Interval = get_interval(Dict),
            case catch duration_to_secs(Interval) of
                Secs when is_integer(Secs) ->
                    ?log("Sleeping ~p seconds.\n", [Secs]),
                    timer:sleep(Secs * 1000),
                    mount_secrets(Dict);
                _ ->
                    ?log("Failed to parse avassa.io/update-interval: ~s\n",
                         [Interval]),
                    ok
            end;
        Other ->
            ?log("Unknown value for avassa.io/mode: ~s\n", [Other]),
            ok
    end.

get_mode(Dict) ->
    case get_opt(<<"mode">>, Dict, undefined)  of
        undefined ->
            case os:getenv("AVASSA_MODE") of
                false ->
                    <<"init">>;
                Mode ->
                    ?l2b(Mode)
            end;
        Mode ->
            Mode
    end.

get_interval(Dict) ->
    case get_opt(<<"update-interval">>, Dict, undefined, _Log=false)  of
        undefined ->
            case os:getenv("AVASSA_UPDATE_INTERVAL") of
                false ->
                    <<"5m">>;
                Interval ->
                    ?l2b(Interval)
            end;
        Interval ->
            Interval
    end.

authenticate(Dict0) ->
    %% Read k8s service account token
    case file:read_file("/var/run/secrets/kubernetes.io/serviceaccount/token")
    of
        {ok, Token} ->
            Dict    = [{<<"token">>, Token}|Dict0],
            Host    = get_opt(<<"api-host">>, Dict),
            Tenant  = get_opt(<<"tenant">>, Dict),
            Service = get_opt(<<"service">>, Dict),
            Role    = get_opt(<<"role">>, Dict, <<"default">>),
            Body = <<"{\"tenant\":\"", Tenant/binary, "\",",
                     "\"service\":\"", Service/binary, "\",",
                     "\"role\":\"", Role/binary, "\",",
                     "\"jwt\":\"", Token/binary, "\"}">>,
            Url = ?io2b(["https://", Host, "/v1/kubernetes-login"]),
            case gun_post(Url, [], <<"application/json">>,
                          Body, tls_opts(Dict)) of
                {ok, _Code, JSONTxt, _Headers} ->
                    JSON = jsx:decode(JSONTxt, [return_maps]),
                    case maps:get(<<"token">>, JSON, undefined) of
                        undefined ->
                            ?log("Failed to authenticate towards control "
                                 "tower: ~p\n", [JSON]),
                            error;
                        AvassaToken ->
                            ?log("Successfully authenticated towards control "
                                 "tower\n", []),
                            AvassaToken
                    end;
                Error ->
                    ?log("Failed to authenticate towards control tower: ~p\n",
                         [Error]),
                    error
            end;
        _ ->
            ?log("Failed to read k8s service account token. Activate "
                 "service account tokens in kubernetes.\n", []),
            <<"">>
    end.

logout(Dict, Token) ->
    Host = get_opt(<<"api-host">>, Dict),
    Url = ?io2b(["https://", Host, "/v1/state/strongbox/token/revoke"]),
    AuthHeader = auth_header(Token),
    Headers = [AuthHeader],
    Body = <<"{}">>,
    case gun_post(Url, Headers, <<"application/json">>, Body, tls_opts(Dict)) of
        {ok, _Code, _JSONTxt, _Headers} ->
            ?log("Successfully logged out from Avassa\n", []);
        Error ->
            ?log("Failed to logout from control tower: ~p\n", [Error]),
            error
    end.

dequote(B) ->
    dequote(?b2l(B), []).

dequote([$\\,$n|Rest], Acc) ->
    dequote(Rest, [$\n|Acc]);
dequote([$\\,$r|Rest], Acc) ->
    dequote(Rest, [$\r|Acc]);
dequote([C|Rest], Acc) ->
    dequote(Rest, [C|Acc]);
dequote([], Acc) ->
    ?l2b(lists:reverse(Acc)).

tls_opts(Dict) ->
    case get_opt(<<"ca-cert">>, Dict, undefined) of
        undefined ->
            #{tls_opts => [{verify, verify_none}]};
        CaCert64 ->
            CaCert = decode_cert(dequote(CaCert64)),
            #{tls_opts => [{verify, verify_peer},
                           {server_name_indication, "api"},
                           {cacerts, [CaCert]}]}
    end.

decode_cert(Cert64) ->
    [{'Certificate', Cert, _}|_] = public_key:pem_decode(Cert64),
    Cert.

get_strongbox_secrets(Dict, Token) ->
    F = fun({<<"vault.",Spec/binary>>, V}, Acc) ->
                case string:tokens(?b2l(Spec), ".") of
                    [Vault, Secret] ->
                        case get_strongbox_secret(Dict, Token, Vault, Secret)
                        of
                            error ->
                                ?log("Failed to get secret: ~s/~s\n",
                                     [Vault, Secret]),
                                Acc;
                            Map ->
                                [{?l2b(Vault), ?l2b(Secret), V, Map}|Acc]
                        end;
                    [Vault, Secret, Key] ->
                        case get_strongbox_secret(Dict, Token, Vault, Secret)
                        of
                            error ->
                                ?log("Failed to get secret: ~s/~s/~s\n",
                                     [Vault, Secret, Key]),
                                Acc;
                            Map ->
                                [{?l2b(Vault), ?l2b(Secret), Key, V, Map}|Acc]
                        end;
                    _ ->
                        ?log("Invalid vault specification: "
                             "avassa.io/vault.~s\n", [Spec]),
                        Acc
                end;
           ({<<"k8s-secrets">>, K8SSecret}, Acc) ->
                get_k8s_secret(Dict, Token, K8SSecret, Acc);
           ({_K,_V},Acc) ->
                Acc
        end,
    lists:foldl(F, [], Dict).

get_k8s_secret(Dict, Token, K8SSecret, Acc) ->
    Host = os:getenv("KUBERNETES_SERVICE_HOST"),
    Port = os:getenv("KUBERNETES_SERVICE_PORT"),
    CaFile = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt",
    {ok, SToken} = file:read_file("/var/run/secrets/kubernetes.io/"
                                  "serviceaccount/token"),
    {ok, Namespace} = file:read_file("/var/run/secrets/kubernetes.io/"
                                     "serviceaccount/namespace"),
    Url = ?io2b(["https://",Host,":",Port,"/api/v1/namespaces/",
                 Namespace,"/secrets/", K8SSecret]),
    TlsOpts = #{tls_opts => [{verify, verify_peer},
                             {cacertfile, CaFile}]},
    AuthHeader = auth_header(SToken),
    CTHeader = {<<"Content-Type">>, <<"application/json">>},
    Headers = [CTHeader, AuthHeader],
    case gun_get(Url, Headers, TlsOpts) of
        {ok, 403, _, _} ->
            ?log("Failed reading k8s secret: permission denied\n", []),
            Acc;
        {ok, _Code, JSONTxt, _Headers} ->
            JSON = jsx:decode(JSONTxt, [return_maps]),
            Data = maps:get(<<"data">>, JSON, #{}),
            case maps:get(<<"strongbox-map">>, Data, undefined) of
                undefined ->
                    Acc;
                Base64Map ->
                    Map = base64:decode(Base64Map),
                    SMap = parse_secret_map(Map),
                    F = fun({Name, Vault, Secret, Key}) ->
                                SDict = get_strongbox_secret(
                                          Dict, Token, Vault, Secret),
                                case maps:get(Key, SDict, undefined) of
                                    undefined ->
                                        ?log("Failed to map:~s/~s/~s\n~p\n",
                                             [Vault, Secret, Key, Map]),
                                        not_found;
                                    Value ->
                                        patch_k8s_secret(
                                          Host, Port, CaFile, Namespace,
                                          SToken, K8SSecret, Name, Value)
                                end
                        end,
                    _Res = [F(M) || M <- SMap],
                    Acc
            end;
        _Other ->
            ?log("Got error from Kubernetes: ~p\n", [_Other]),
            Acc
    end.

patch_k8s_secret(Host, Port, CaFile, Namespace,
                 SToken, K8SSecret, Name, Value) ->
    AuthHeader = auth_header(SToken),
    Headers = [AuthHeader],
    Value64 = base64:encode(Value),
    JSON = [#{ <<"op">>    => <<"replace">>,
               <<"path">>  => <<"/data/",Name/binary>>,
               <<"value">> => Value64 }],
    Body = jsx:encode(JSON),
    Url = ?io2b(["https://",Host,":",Port,"/api/v1/namespaces/",
                 Namespace,"/secrets/", K8SSecret]),
    TlsOpts = #{tls_opts => [{verify, verify_peer},
                             {cacertfile, CaFile}]},
    case gun_patch(Url, Headers, <<"application/json-patch+json">>, Body,
                   TlsOpts) of
        {ok, _Code, _RepBody, _Headers} ->
            ?log("Updated kubernetes secrets\n", []),
            ok;
        _Failed ->
            ?log("Update of kubernetes secrets failed\n", []),
            error
    end.

parse_secret_map(Bin) ->
    Lines = string:split(Bin, <<"\n">>, all),
    parse_secret_map_lines(Lines, []).

parse_secret_map_lines([], Acc) ->
    Acc;
parse_secret_map_lines([Line|Lines], Acc) ->
    case string:split(Line, <<":">>) of
        [Name, Path] ->
            case string:split(string:trim(Path, both), <<"/">>, all) of
                [Vault, Secret, Key] ->
                    parse_secret_map_lines(
                      Lines, [{Name, Vault, Secret, Key}|Acc]);
                _Other ->
                    ?log("Malformed annotation: ~p\n", [_Other]),
                    parse_secret_map_lines(Lines, Acc)
            end;
        _Other ->
            ?log("Malformed annotation: ~p\n", [_Other]),
            parse_secret_map_lines(Lines, Acc)
    end.

get_strongbox_secret(Dict, Token, Vault, Secret) ->
    Host = get_opt(<<"api-host">>, Dict),
    Url = ?io2b(["https://", Host, "/v1/state/strongbox/vaults/",
                 Vault, "/secrets/", Secret]),
    AuthHeader = auth_header(Token),
    CTHeader = {<<"Content-Type">>, <<"application/json">>},
    Headers = [CTHeader, AuthHeader],
    case gun_get(Url, Headers, tls_opts(Dict)) of
        {ok, 403, _, _} ->
            ?log("Failed to read Strongbox secret: permission denied\n", []),
            error;
        {ok, _Code=404, _, _} ->
            ?log("Failed to read Strongbox secret: secret not found\n", []),
            error;
        {ok, _Code=200, JSONTxt, _Headers} ->
            JSON = jsx:decode(JSONTxt, [return_maps]),
            maps:get(<<"dict">>, JSON);
        Error ->
            ?log("Failed to read Strongbox secret: ~p\n", [Error]),
            error
    end.

auth_header(Token) ->
    {<<"Authorization">>, <<"Bearer ", Token/binary>>}.

write_secrets(_Dict, []) ->
    ok;
write_secrets(Dict, [{_V,_S, <<"files">>, Map}|Secrets]) ->
    ?log("Files ~p\n", [Map]),
    CWD = <<"/avassa/secrets">>,
    F = fun(K,V) ->
                case filelib:safe_relative_path(K, CWD) of
                    unsafe ->
                        ?log("Error: unsafe filename: ~s\n", [K]);
                    Normalized ->
                        case filename:split(Normalized) of
                            [_] ->
                                F = filename:join(CWD, Normalized),
                                case safe_write_file(F, V) of
                                    ok ->
                                        ok;
                                    Error ->
                                        ?log("Error: failed to write ~s: ~p\n",
                                             [F, Error])
                                end;
                            _ ->
                                ?log("Error: multilevel filename: ~s\n",
                                     [K])
                        end
                end
        end,
    maps:foreach(F, Map),
    write_secrets(Dict, Secrets);
write_secrets(Dict, [{_V,_S, <<"json/",File/binary>>, Map}|Secrets]) ->
    JSON = jsx:encode(Map),
    CWD = <<"/avassa/secrets">>,
    case filelib:safe_relative_path(File, CWD) of
        unsafe ->
            ?log("Error: unsafe filename: ~s\n", [File]);
        Normalized ->
            F = filename:join(CWD, Normalized),
            case safe_write_file(F, JSON) of
                ok ->
                    ok;
                Error ->
                    ?log("Error: failed to write ~s: ~p\n",
                         [F, Error])
            end
    end,
    write_secrets(Dict, Secrets);
write_secrets(Dict, [{_V,_S, Key, File, Map}|Secrets]) ->
    ?log("File Map: ~p\n", [{Key, File, Map}]),
    Value = maps:get(?l2b(Key), Map),
    CWD = <<"/avassa/secrets">>,
    case filelib:safe_relative_path(File, CWD) of
        unsafe ->
            ?log("Error: unsafe filename: ~s\n", [File]);
        Normalized ->
            F = filename:join(CWD, Normalized),
            case safe_write_file(F, Value) of
                ok ->
                    ok;
                Error ->
                    ?log("Error: failed to write ~s: ~p\n",
                         [F, Error])
            end
    end,
    write_secrets(Dict, Secrets);
write_secrets(Dict, [_Ignore|Secrets]) ->
    ?log("Ignoring ~p\n", [_Ignore]),
    write_secrets(Dict, Secrets).

get_opt(Opt, [], Default, Log) ->
    if Log ->
            ?log("Missing from annotations: avassa.io/~s\n", [Opt]);
       true ->
            ok
    end,
    Default;
get_opt(Opt, [{Opt, Value}|_], _Default, _Log) ->
    if is_binary(Value) ->
            Value;
       is_list(Value) ->
            ?l2b(Value);
       true ->
            Value
    end;
get_opt(Opt, [_|Dict], Default, Log) ->
    get_opt(Opt, Dict, Default, Log).

get_opt(Opt, Dict, Default) ->
    get_opt(Opt, Dict, Default, _Log=true).

get_opt(Opt, Dict) ->
    get_opt(Opt, Dict, <<"unknown">>).

parse_annotations() ->
    case file:read_file("/avassa/podinfo/annotations") of
        {ok, A} ->
            parse_annotations(string:split(A, <<"\n">>, all), []);
        _ ->
            ?log("No annotations file at: /avassa/podinfo/annotations. "
                 "Ensure that metadata.annotations is mounted at the "
                 "/avassa/podinfo path.\n",[]),
            []
    end.

parse_annotations([], Acc) ->
    Acc;
parse_annotations([Line|Lines], Acc) ->
    case string:split(Line, <<"=">>) of
        [<<"avassa.io/", Key/binary>>, Value] ->
            parse_annotations(Lines, [{trim(Key), trim(Value)}|Acc]);
        _ ->
            %% ignore other annotations
            parse_annotations(Lines, Acc)
    end.

trim(Str0) ->
    Str = ?b2l(Str0),
    ?l2b(string:strip(string:trim(Str, both), both, $")).

safe_write_file(Filename, Bytes) ->
    TmpFile = <<Filename/binary, ".amstmp">>,
    case file:write_file(TmpFile, Bytes) of
        ok ->
            file:rename(TmpFile, Filename);
        Error ->
            Error
    end.

duration_to_secs(Str) ->
    {Y,D,H,M,S} = parse_duration(Str),
    (((365*Y+D)*24+H)*60+M)*60+S.

parse_duration(Duration) ->
    parse_duration(?b2l(Duration), [], 0, {0,0,0,0,0}).

-define(Y, 1).
-define(D, 2).
-define(H, 3).
-define(M, 4).
-define(S, 5).

parse_duration([], [], _, Duration) ->
    Duration;
parse_duration([$y|Rest], Acc, P, {0,D,H,M,S}) when P < ?Y->
    Y = ?l2i(lists:reverse(Acc)),
    parse_duration(Rest, [], ?Y, {Y,D,H,M,S});
parse_duration([$d|Rest], Acc, P, {Y,0,H,M,S}) when P < ?D ->
    D = ?l2i(lists:reverse(Acc)),
    parse_duration(Rest, [], ?D, {Y,D,H,M,S});
parse_duration([$h|Rest], Acc, P, {Y,D,0,M,S}) when P < ?H ->
    H = ?l2i(lists:reverse(Acc)),
    parse_duration(Rest, [], ?H, {Y,D,H,M,S});
parse_duration([$m|Rest], Acc, P, {Y,D,H,0,S}) when P < ?M->
    M = ?l2i(lists:reverse(Acc)),
    parse_duration(Rest, [], ?M, {Y,D,H,M,S});
parse_duration([$s|Rest], Acc, P, {Y,D,H,M,0}) when P < ?S ->
    S = ?l2i(lists:reverse(Acc)),
    parse_duration(Rest, [], ?S, {Y,D,H,M,S});
parse_duration([C|Rest], Acc, P, Duration) ->
    parse_duration(Rest, [C|Acc], P, Duration).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GUN

gun_get(Url, Headers, ConnectionOpts) ->
    gun_get(Url, Headers, ConnectionOpts,
            fun(Conn, Ref) ->
                    case collect_response(Conn, Ref, 0, [], []) of
                        {ok, Code, Data, RespHeaders} ->
                            gun:close(Conn),
                            {ok, Code, Data, RespHeaders};
                        Err ->
                            gun:close(Conn),
                            Err
                    end
            end).

gun_get(Url, Headers, ConnectionOpts, RespFun) ->
    try
        case gun_open_await_up(Url, ConnectionOpts) of
            {ok, Conn, Path} ->
                Ref = gun:get(Conn, Path, Headers),
                RespFun(Conn, Ref);
            Err ->
                Err
        end
    catch
        X:Y:Stacktrace ->
            ?log("~p:~p\n~p\n", [X,Y,Stacktrace]),
            {error, crashed}
    end.

gun_post(Url, Headers0, ContentType, Payload, ConnectionOpts) ->
    gun_post(Url, Headers0, ContentType, Payload, ConnectionOpts,
             fun(Conn, Ref) ->
                     case collect_response(Conn, Ref, 0, [], []) of
                         {ok, Code, Data, RespHeaders} ->
                             gun:close(Conn),
                             {ok, Code, Data, RespHeaders};
                         Err ->
                             gun:close(Conn),
                             Err
                     end
             end).

gun_post(Url, Headers0, ContentType, Payload, ConnectionOpts, RespFun) ->
    ContentTypeHeader = {<<"content-type">>, ContentType},
    Headers = [ContentTypeHeader|Headers0],
    gun_post2(Url, Headers, Payload, ConnectionOpts, RespFun).

gun_post2(Url, Headers, Payload, ConnectionOpts, RespFun) ->
    try
        case gun_open_await_up(Url, ConnectionOpts) of
            {ok, Conn, Path} ->
                Ref = gun:post(Conn, Path, Headers, Payload),
                RespFun(Conn, Ref);
            Err ->
                Err
        end
     catch
         _X:_Y:_Stacktrace ->
             ?log("~p:~p\n~p\n", [_X,_Y,_Stacktrace]),
             {error, crashed}
     end.

gun_patch(Url, Headers0, ContentType, Payload, ConnectionOpts) ->
    gun_patch(Url, Headers0, ContentType, Payload, ConnectionOpts,
             fun(Conn, Ref) ->
                     case collect_response(Conn, Ref, 0, [], []) of
                         {ok, Code, Data, RespHeaders} ->
                             gun:close(Conn),
                             {ok, Code, Data, RespHeaders};
                         Err ->
                             gun:close(Conn),
                             Err
                     end
             end).

gun_patch(Url, Headers0, ContentType, Payload, ConnectionOpts, RespFun) ->
    ContentTypeHeader = {<<"content-type">>, ContentType},
    Headers = [ContentTypeHeader|Headers0],
    gun_patch2(Url, Headers, Payload, ConnectionOpts, RespFun).

gun_patch2(Url, Headers, Payload, ConnectionOpts, RespFun) ->
    try
        case gun_open_await_up(Url, ConnectionOpts) of
            {ok, Conn, Path} ->
                Ref = gun:patch(Conn, Path, Headers, Payload),
                RespFun(Conn, Ref);
            Err ->
                Err
        end
     catch
         _X:_Y:_Stacktrace ->
             ?log("~p:~p\n~p\n", [_X,_Y,_Stacktrace]),
             {error, crashed}
     end.


gun_open_await_up(Url, ConnectionOpts0) ->
    %% NOTE: special option to ignore adding other TLS opts
    IgnoreExtraTlsOpts = maps:get(ignore_tls_opts, ConnectionOpts0, false),
    ConnectionOpts = maps:remove(ignore_tls_opts, ConnectionOpts0),
    URIMap = uri_string:parse(Url),
    case maps:get(scheme, URIMap) of
        <<"http">> ->
            Port = maps:get(port, URIMap, 80),
            Transport = ConnectionOpts;
        <<"https">> when IgnoreExtraTlsOpts == true ->
            Port = maps:get(port, URIMap, 443),
            Transport = ConnectionOpts;
        <<"https">> ->
            Port = maps:get(port, URIMap, 443),
            Transport = set_tls_opts(ConnectionOpts)
    end,
    Host = uri_map_host(URIMap),
    {ok, Conn} = gun:open(Host, Port, Transport),
    case gun:await_up(Conn) of
        {ok, _Proto} ->
            Path = compose_path(URIMap),
            {ok, Conn, Path};
        Err ->
            gun:close(Conn),
            Err
    end.

%% do_collect_response(Conn, Ref) ->
%%     case collect_response(Conn, Ref, 0, [], []) of
%%         {ok, Code, Data, RespHeaders} ->
%%             gun:close(Conn),
%%             {ok, Code, Data, RespHeaders};
%%         Err ->
%%             gun:close(Conn),
%%             Err
%%     end.

collect_response(Conn, Ref, RetCode, SoFar, Headers0) ->
    case gun:await(Conn, Ref, timer:minutes(1)) of
        {data, IsFin, Data} ->
            case IsFin of
                nofin ->
                    collect_response(Conn, Ref, RetCode, [SoFar,Data],Headers0);
                fin ->
                    {ok, RetCode, iolist_to_binary([SoFar,Data]), Headers0}
            end;
        {response, IsFin, Code, Headers1} ->
            Headers = Headers0 ++ Headers1,
            case IsFin of
                nofin ->
                    collect_response(Conn, Ref, Code, SoFar, Headers);
                fin ->
                    {ok, Code, iolist_to_binary(SoFar), Headers}
            end;
        _Err ->
            failed
    end.

set_tls_opts(ConnectionOpts) ->
    TlsOpts1 =  maps:get(tls_opts, ConnectionOpts, []),
    %% set verify
    case proplists:get_value(verify, TlsOpts1, undefined) of
        undefined ->
            TlsOpts2 = [{verify, verify_peer}|TlsOpts1];
        _Verify ->
            TlsOpts2 = TlsOpts1
    end,
    case proplists:get_value(verify, TlsOpts2, undefined) of
        undefined ->
            Verify = false;
        verify_none ->
            Verify = false;
        verify_peer ->
            Verify = true
    end,
    case proplists:get_value(cacerts, TlsOpts2, undefined) of
        undefined ->
            TlsOpts3 = TlsOpts2;
        CaCerts when Verify ->
            %% note that the caller must include the root bundle in cacerts
            %% if the full chain isn't included in cacerts
            DecodedCaCerts = [public_key:pkix_decode_cert(Cert, otp) ||
                                 Cert <- CaCerts],
            %% set tls transport
            Fun = {verify_fun, {fun(Cert, {bad_cert, selfsigned_peer}, US) ->
                                        case lists:member(Cert, DecodedCaCerts)
                                        of
                                            true ->
                                                {valid, US};
                                            false ->
                                                {fail, {bad_cert,
                                                        selfsigned_peer}}
                                        end;
                                   (_, {bad_cert, _} = Reason, _) ->
                                        {fail, Reason};
                                   (_, {extension, _}, UserState) ->
                                        {unknown, UserState};
                                   (_, valid, UserState) ->
                                        {valid, UserState};
                                   (_, valid_peer, UserState) ->
                                        {valid, UserState}
                        end, []}},
            TlsOpts3 = [Fun|TlsOpts2];
        _ ->
            TlsOpts3 = delete_props([server_name_indication,
                                     cacerts, verify_fun], TlsOpts2)
    end,
    CustomFun = pkix_verify_hostname_match_fun(https),
    NewTlsOpts = [{customize_hostname_check,
                   [{match_fun, CustomFun}]}|TlsOpts3],
    ConnectionOpts#{transport => tls, tls_opts => NewTlsOpts}.

pkix_verify_hostname_match_fun(https) ->
    CustomFun = public_key:pkix_verify_hostname_match_fun(https),
    fun(X={dns_id, DnsId}, Y={iPAddress, IP=[_|_]}) ->
            case inet:ntoa(?l2t(IP)) of
                DnsId ->
                    true;
                _Other ->
                    CustomFun(X,Y)
            end;
       (X, Y) ->
            CustomFun(X,Y)
    end.

delete_props([], PropList) ->
    PropList;
delete_props([Opt|Opts], PropList) ->
    NewPropList = proplists:delete(Opt, PropList),
    delete_props(Opts, NewPropList).

compose_path(Map) ->
    case maps:get(path, Map, <<"/">>) of
        <<>> ->
            Base = <<"/">>;
        Base ->
            ok
    end,
    case maps:get(query, Map, undefined) of
        undefined ->
            Base;
        Query ->
            <<Base/binary,"?",Query/binary>>
    end.

uri_map_host(#{host := Host}) when is_binary(Host) ->
    unicode:characters_to_list(Host);
uri_map_host(#{host := Host}) ->
    Host.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

log(_M,L,F,A) ->
    io:format("line ~p: "++F,[L|A]),
    ok.

