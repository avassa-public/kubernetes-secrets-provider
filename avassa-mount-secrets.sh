#!/usr/local/bin/escript
%%! -pz gun/ebin cowlib/ebin jsx/ebin ams/ebin
%% SPDX-License-Identifier: Apache-2.0

-export([main/1]).

main(_) ->
    ams:start().

