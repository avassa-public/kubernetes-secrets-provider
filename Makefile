ERL = $(wildcard *.erl)
BEAM = $(patsubst %.erl, %.beam, $(ERL))

DEPS     = cowlib gun jsx

dep_cowlib = git https://github.com/ninenines/cowlib 2.11.0
dep_gun = git https://github.com/avassa-io/gun.git

build: deps
	$(MAKE) -C ams all
	docker build -t avassa-mount-secrets:0.1 .

realclean: clean
	rm -rf deps .erlang.mk
	$(MAKE) -C ams clean

include mk/stderl.mk

